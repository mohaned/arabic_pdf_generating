#!/usr/bin/env python
# -*- coding: utf-8 -*-

import fpdf
import arabic_reshaper

from bidi.algorithm import get_display

def cer_fun():
    inch = 72.0
    text = "كورس عربي"

    reshaped_text = arabic_reshaper.reshape(u"كورس عربي")
    bidi_text = get_display(reshaped_text)

    # pdf = fpdf.FPDF(format='letter')
    pdf = fpdf.FPDF('L', 'in', 'Letter')
    pdf.add_page()
    pdf.add_font('DejaVu', '', 'DejaVuSansCondensed.ttf', uni=True)
    pdf.set_font('DejaVu', '', 14)

    # pdf.cell(200, 10, txt=bidi_text.encode('UTF-8'))
    pdf.image('cert.png', x=0, y=0.12, w=11)
    pdf.cell(1, txt=bidi_text)
    pdf.output("tutorial.pdf")


def certificate_gen(image_path, cer_path, font_path):
    inch = 72.0
    text = "كورس عربي"

    reshaped_text = arabic_reshaper.reshape(u"كورس عربي")
    bidi_text = get_display(reshaped_text)

    # pdf = fpdf.FPDF(format='letter')
    pdf = fpdf.FPDF('L', 'in', 'Letter')
    pdf.add_page()
    # pdf.add_font('DejaVu', '', font_path, uni=True)
    # pdf.set_font('DejaVu', '', 14)

    # pdf.cell(200, 10, txt=bidi_text.encode('UTF-8'))
    pdf.image(image_path, x=0, y=0.12, w=11)
    # pdf.cell(1, txt=bidi_text)
    pdf.output(cer_path)

# cer_fun()

"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import fpdf
import arabic_reshaper

from bidi.algorithm import get_display

inch = 72.0
text = "كورس عربي"

reshaped_text = arabic_reshaper.reshape(u"كورس عربي")
bidi_text = get_display(reshaped_text)

# pdf = fpdf.FPDF(format='letter')
pdf = fpdf.FPDF('L', 'in', 'Letter')
pdf.add_page()
pdf.add_font('DejaVu', '', 'DejaVuSansCondensed.ttf', uni=True)
pdf.set_font('DejaVu', '', 14)

# pdf.cell(200, 10, txt=bidi_text.encode('UTF-8'))
pdf.image('cert.png', x=0, y=0.12, w=11)

pdf.output("tutorial.pdf")

"""
